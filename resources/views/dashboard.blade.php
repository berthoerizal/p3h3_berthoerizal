@extends('layout.master')
@section('judul', 'Dashboard')
@section('content')
   <ul class="list-group">
       <li class="list-group-item"><a href="/table"><h5>Halaman Table</h5></a></li>
       <li class="list-group-item"><a href="/data-table"><h5>Halaman Datatable</h5></a></li>
   </ul>
@endsection