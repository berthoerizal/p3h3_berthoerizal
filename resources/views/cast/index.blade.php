@extends('layout.master')
@section('judul', 'Cast')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <i class="icon fas fa-check"></i> {{$message}}
</div>
@endif
<a href="/cast/create" class="btn btn-primary mb-2">Tambah</a>
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>#</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Bio</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key=>$item)
      <tr>
        <td>{{$key+1}}</td>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>{{$item->bio}}</td>
        <td>
          <a href="/cast/{{$item->id}}" class="btn btn-info">Detail</a>
          <a href="/cast/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
          @include('cast.hapus_modal')
        </td>
      </tr>
      @empty
          <tr>
            <td colspan="5" class="text-center">Data Empty</td>
          </tr>
      @endforelse
    
    </tbody>
  </table>
@endsection