@extends('layout.master')
@section('judul', 'Detail Cast | '.$cast->nama)

@section('content')
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>Nama</td>
                <td>{{$cast->nama}}</td>
                <td rowspan="3" class="text-center"><img src="{{asset('admin/dist/img/default_photoprofile.jpeg')}}" alt="Default Photo Profile" class="img img-thumbnail" width="200px"></td>
            </tr>
            <tr>
                <td>Umur</td>
                <td>{{$cast->umur}}</td>
            </tr>
            <tr>
                <td>Bio</td>
                <td>{{$cast->bio}}</td>
            </tr>
        </table>
    </div>
@endsection