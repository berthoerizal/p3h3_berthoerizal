<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\Cast;

class CastController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $cast = DB::table('cast')->get();
        $cast = Cast::all();
        return view('cast.index', ['cast' => $cast]);
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // $cast = DB::table('cast')->insert([
        //     'nama' => $request->nama,
        //     'umur' => $request->umur,
        //     'bio' => $request->bio
        // ]);

        $cast = Cast::create([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        if ($cast) {
            return redirect("/cast")->with("success", "Data berhasil ditambah!");
        }
    }

    public function show($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::findOrFail($id);
        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::findOrFail($id);
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // $cast = DB::table('cast')->where('id', $id)->update([
        //     'nama' => $request->nama,
        //     'umur' => $request->umur,
        //     'bio' => $request->bio
        // ]);

        $cast = Cast::findOrFail($id);
        $cast->update([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        if ($cast) {
            return redirect('/cast')->with('success', 'Data berhasil diubah');
        }
    }

    public function destroy($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->delete();
        $cast = Cast::findOrFail($id);
        $cast->delete();
        if ($cast) {
            return redirect('/cast')->with('success', 'Data berhasil dihapus');
        }
    }
}
